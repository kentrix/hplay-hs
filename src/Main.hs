{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables, RecordWildCards #-}
{-# LANGUAGE ExtendedDefaultRules #-}

module Main where

import qualified Control.Concurrent             as Concurrent
import qualified Control.Exception              as Exception
import qualified Control.Monad                  as Monad
import qualified Control.Concurrent.Async       as Async
import qualified Data.List                      as List
import qualified Data.Maybe                     as Maybe
import qualified Data.Text                      as Text
import qualified Data.Text.Lazy.Encoding             as Encoding
import qualified Data.ByteString.Lazy           as BL
import qualified Data.ByteString.Lazy.Internal           as BL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Internal as BS
import qualified Data.ByteString.Char8 as BC
import qualified Network.Wai.Handler.WebSockets as WS
import qualified Network.WebSockets             as WS
import qualified Safe
import qualified Control.Concurrent.STM         as STM
import qualified Network.MPD                    as MPD
import qualified Control.Monad.Trans.Error      as ET
import qualified Data.String as S
import Data.Aeson
import Json
import Mpdinterface
import Downloader

type ClientID = Int
type Client = (ClientID, WS.Connection)
type ClientStates = [Client]

addClient :: Client -> ClientStates -> ClientStates
addClient x xs = x:xs

removeClient :: ClientID -> ClientStates -> ClientStates
removeClient x = filter ((/= x) . fst )

withoutClient :: ClientID -> ClientStates -> ClientStates
withoutClient clientId = filter ((/=) clientId . fst)

issueID :: ClientStates -> ClientID
issueID = Maybe.maybe 0 ((+) 1) . Safe.maximumMay . fmap fst

broadcast :: BL.ByteString -> ClientStates -> IO ()
broadcast message clients = do
 {- Monad.forM_ clients $ \(i, conn) -> putStrLn (show i) -}
    Monad.forM_ clients $ \(_, conn) -> WS.sendTextData conn message

mpdIdler :: STM.TVar ClientStates -> IO ()
mpdIdler sts =
    Monad.forever $ do
        res <- MPD.withMPD $ MPD.idle subsysToListen
        cs <- STM.atomically $ do
            clients <- STM.readTVar sts
            return clients
        sd <- getClientUpdateData' res
        flip broadcast cs $ encode sd


main :: IO ()
main = do
  clientStates <- STM.atomically $ STM.newTVar []
  Concurrent.forkIO $ mpdIdler clientStates
  chan <- STM.atomically STM.newBroadcastTChan
  WS.runServer "127.0.0.1" 8080 $ application clientStates chan

application :: STM.TVar ClientStates -> STM.TChan [MPD.Subsystem] -> WS.PendingConnection -> IO ()
application states chan pending = do
    conn <- WS.acceptRequest pending
    WS.forkPingThread conn 30
    msg :: Text.Text  <- WS.receiveData conn
    readChan <- STM.atomically $ do 
        STM.dupTChan chan
    c2 <- STM.atomically $ do
        innerS <- STM.readTVar states
        let client = (issueID innerS, conn)
        STM.modifyTVar states $ \s ->
            addClient client s
        return client
    flip Exception.finally (flip disconnect states $ fst c2) $ do
        putStrLn "Client Connect"
        sendInitToClients [conn]
        loop conn states c2 readChan
    
loop :: WS.Connection -> STM.TVar ClientStates -> Client -> STM.TChan [MPD.Subsystem] -> IO ()
loop conn sts client readChan =
    Async.race_
        (Monad.forever $ do
            channelData <- STM.atomically $ do
                d <- STM.readTChan readChan
                return d
            sendInitToClients [conn]
        )
        (Monad.forever $ do
            msg <- WS.receiveData conn
            BL.putStrLn msg
            decodeAndAct msg
            s <- STM.atomically $ do
                d <- STM.readTVar sts
                return d
            return ()
        )

decodeAndAct :: BL.ByteString -> IO ()
decodeAndAct d =
    let ded = (decode $ d) :: Maybe ClientData
    in
        case ded of Just a -> carryOut a
                    Nothing -> return ()

forkUpdate :: FilePath -> IO ()
forkUpdate p = do
    {- we listen to MPD events and check if its still updating -}
    MPD.withMPD $ MPD.idle subsysToListen
    res <- MPD.withMPD $ MPD.status
    case res of
        Right MPD.Status{..} ->
            case stUpdatingDb of
                Just _ -> forkUpdate p
                _ -> do
                    putStrLn $ "added" ++ show p
                    res <- MPD.withMPD $ MPD.add $ S.fromString p
                    putStrLn $ show res
        _ -> return ()
    {- Some concerning around this, we could possibly miss an event  -}
    

carryOut :: ClientData -> IO ()
carryOut all@ClientData{..} =
    case rType of
        RPlay -> do
            MPD.withMPD $ MPD.play rInt
            return ()
        RPause -> do 
            MPD.withMPD $ MPD.pause True
            return ()
        RSeek -> do
            pos <- mpdCurrSongPos
            putStrLn $ show pos
            case pos of 
                Just n -> case rInt of 
                    Just m -> do 
                        MPD.withMPD $ MPD.seek n (fromIntegral m)
                        return ()
                    _ -> return ()
                _ -> return ()
        RNew ->
            case clientDataToDownloadConfig all of
                Just cfg -> do
                    putStrLn $ show cfg
                    res <- downloadFile cfg
                    case res of 
                        DOk p -> do 
                            putStrLn $ "adding" ++ show p
                            MPD.withMPD $ MPD.update Nothing
                            Concurrent.forkIO $ forkUpdate p
                            return ()
                        _ -> return ()
                    return ()
                _ -> return ()
        RDel -> do
            case rInt of
                Just n -> do
                    MPD.withMPD $ MPD.delete n
                    return ()
                _ -> return ()
                
        _ -> return ()

disconnect :: ClientID -> STM.TVar ClientStates -> IO ()
disconnect id states = do 
    STM.atomically $ do
        STM.modifyTVar states $ \s ->
            removeClient id s

sendInitToClients :: [WS.Connection] -> IO ()
sendInitToClients conns = do
    res <- MPD.withMPD $ MPD.status {- use transformer for this? -}
    songList <- MPD.withMPD $ MPD.playlistInfo Nothing
    library <- MPD.withMPD $ MPD.lsInfo ""
    sendInitToClients' songList library res conns

sendInitToClients' :: MPD.Response [MPD.Song]
                    -> MPD.Response [MPD.LsResult]
                    -> MPD.Response MPD.Status
                    -> [WS.Connection]
                    -> IO ()
sendInitToClients' a b c conns =
    let cb = mkInitData a b c
    in
        case cb of Just s -> Monad.forM_ conns $ \x -> 
                                WS.sendTextData x $ encode s
                   Nothing ->Monad.forM_ conns $ \x -> 
                                WS.sendTextData x $ encode errServerData

errServerData :: ServerData
errServerData = 
    ServerData { iType = IError
                , iDetail = Nothing
                , iSongList = Nothing
                , iNotification = Nothing
                , iLibrary = Nothing
                , iPlayerStatus = Nothing
                , iVolume = Nothing
                , iCurrTime = Nothing
                , iCurrPos = Nothing
                }


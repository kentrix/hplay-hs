{-# LANGUAGE OverloadedStrings, RecordWildCards #-}
module Mpdinterface where
import qualified Network.MPD as MPD
import Control.Monad (forever, foldM, when)
import Control.Concurrent.STM (TChan, TVar, atomically, readTVar)
import Json
import qualified Control.Monad.Trans.Error as ET
import Data.Maybe (catMaybes)
import qualified Data.Aeson as AE
import qualified Data.Text as Text
import qualified Data.Text.Encoding as EN
import qualified Data.Text.Lazy.Encoding as LEN
import qualified Data.ByteString as BS

subsysToListen :: [MPD.Subsystem]
subsysToListen = [ MPD.DatabaseS
                 , MPD.UpdateS
                 , MPD.StoredPlaylistS
                 , MPD.PlaylistS
                 , MPD.PlayerS
                 , MPD.MixerS
                 , MPD.OutputS
                 , MPD.OptionsS ] 

statusSubsys :: [MPD.Subsystem]
statusSubsys = [ MPD.PlaylistS, MPD.PlayerS, MPD.MixerS, MPD.OutputS, MPD.OptionsS ]

playlistSubsys :: [MPD.Subsystem]
playlistSubsys = [ MPD.PlaylistS ]

librarySubsys :: [MPD.Subsystem]
librarySubsys = [ MPD.UpdateS, MPD.DatabaseS ]

af :: [MPD.Subsystem] -> [MPD.Subsystem] -> Bool
af a b = any (\x -> x `elem` b) a

baseServerData :: ServerData
baseServerData = ServerData IUpdate Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing

getClientUpdateData' :: Either MPD.MPDError [MPD.Subsystem] -> IO ServerData
getClientUpdateData' (Right sss) = getClientUpdateData sss
getClientUpdateData' (Left _) = return baseServerData


getClientUpdateData :: [MPD.Subsystem] -> IO ServerData
getClientUpdateData ss = do
    let sd = baseServerData
    sd1 <- if (af ss statusSubsys) then (updateStatus' sd) else return sd
    sd2 <- if (af ss playlistSubsys) then (updatePlaylist' sd1) else return sd1
    sd3 <- if (af ss librarySubsys) then (updateLibrary' sd2) else return sd2
    return sd3

updateStatus' :: ServerData -> IO ServerData
updateStatus' a@ServerData{..} = do
    s <- MPD.withMPD $ MPD.status
    return (case s of
        Right s_ -> 
            let (s1',s2,s3,s4) = status2ClientStatus s_
                s1 = mpdStatus2Status s1'
            in a { iPlayerStatus = Just s1 
              , iVolume = s2
              , iCurrTime = s3
              , iCurrPos = s4
              }
        Left e_ ->
            a { iType = IError 
              , iDetail = Just . Text.pack $ show e_
              } )

updateStatus :: ServerData -> MPD.Status -> ServerData
updateStatus a@ServerData{..} status = do
    a { iPlayerStatus = Just s1 
      , iVolume = s2
      , iCurrTime = s3
      , iCurrPos = s4
    }
    where (s1',s2,s3,s4) = status2ClientStatus status
          s1 = mpdStatus2Status s1'

updatePlaylist' :: ServerData -> IO ServerData
updatePlaylist' a@ServerData{..} = do
    s <- MPD.withMPD $ MPD.playlistInfo Nothing
    return (case s of
        Right s_ ->
            a { iSongList = Just $ map song2ClientSong s_}
        Left e_ -> 
            a { iType = IError 
              , iDetail = Just . Text.pack $ show e_
              } )

updatePlaylist :: ServerData -> [MPD.Song] -> ServerData
updatePlaylist a@ServerData{..} songs = a { iSongList = Just $ map song2ClientSong songs }

updateLibrary' :: ServerData -> IO ServerData
updateLibrary' a@ServerData{..} = do
    s <- MPD.withMPD $ MPD.lsInfo ""
    return (case s of
        Right s_ ->
            a { iLibrary = Just $ catMaybes $ map lsResult2ClientSong s_}
        Left e_ ->
            a { iType = IError 
              , iDetail = Just . Text.pack $ show e_
              } )

updateLibrary :: ServerData -> [MPD.LsResult] -> ServerData
updateLibrary a@ServerData{..} res = 
    a { iLibrary = Just $ catMaybes $ map lsResult2ClientSong res }


mkInitData :: MPD.Response [MPD.Song]
                    -> MPD.Response [MPD.LsResult]
                    -> MPD.Response MPD.Status
                    -> Maybe ServerData
mkInitData (Right a) (Right b) (Right c) =
    Just $ 
        ServerData { iType = IUpdate
                   , iDetail = Nothing
                   , iSongList = Just $ map song2ClientSong a
                   , iNotification = Nothing
                   , iLibrary = Just . mkIds . catMaybes $ map lsResult2ClientSong b
                   , iPlayerStatus = Just s1
                   , iVolume = s2
                   , iCurrTime = s3
                   , iCurrPos = s4
                   }
    where (s1',s2,s3,s4) = status2ClientStatus c
          s1 = mpdStatus2Status s1'
mkInitData _ _ _ = Nothing


status2ClientStatus :: MPD.Status -> (MPD.State, Maybe Int, Maybe Int, Maybe Int)
status2ClientStatus MPD.Status{..} =
    (stState, stVolume, ct stTime, stSongPos)
    where ct (Just (s, _)) = Just $ round s
          ct _ = Nothing

mkIds :: [ClientSongItem] -> [ClientSongItem]
mkIds = zipWith (flip mkIds') [0..]

mkIds' :: ClientSongItem -> Int -> ClientSongItem
mkIds' (all@ClientSongItem{..}) n = all { pos = Just n } 

lsResult2ClientSong :: MPD.LsResult -> Maybe ClientSongItem
lsResult2ClientSong (MPD.LsSong s) = Just $ song2ClientSong s
lsResult2ClientSong _ = Nothing

song2ClientSong :: MPD.Song -> ClientSongItem
song2ClientSong MPD.Song{..} =
    ClientSongItem { pos = sgIndex
                   , name = EN.decodeUtf8 $ MPD.toUtf8 sgFilePath 
                   , len = fromIntegral sgLength
                   , origin = " "}


mpdStatus2Status :: MPD.State -> Status
mpdStatus2Status s = 
    case s of
        MPD.Playing -> Playing
        MPD.Stopped -> Stopped
        MPD.Paused -> Paused

mpdCurrSongPos :: IO (Maybe Int)
mpdCurrSongPos = do
    res <- MPD.withMPD $ MPD.status
    return $ extractSongPos res

extractSongPos :: MPD.Response MPD.Status -> Maybe Int
extractSongPos (Right MPD.Status{..}) = stSongPos
extractSongPos _ = Nothing

{-
findPathByName :: String -> 
findPathByName = do
    s <- MPD.withMPD $ MPD.lsInfo ""
    return (case s of
        Right s_ ->
            a { iLibrary = Just $ catMaybes $ map lsResult2ClientSong s_}
        Left e_ ->
            a { iType = IError 
              , iDetail = Just . Text.pack $ show e_
              } )
-}

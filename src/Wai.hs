{-# LANGUAGE OverloadedStrings #-}

-- | Wai+Warp file server. Used GHC 7.4, Wai 1.1, Warp 1.1 versions.
module Main where

import Network.Wai.Handler.Warp (run)
import Network.Wai.Application.Static 
  ( StaticSettings(..)
  , staticApp
  , defaultWebAppSettings
  )

-- | Start Warp server on port 8080.
main :: IO ()
main = do
  putStrLn $ "http://*:8080/"
  run 8080 $ staticApp $ defaultWebAppSettings "/home/e/Downloads"

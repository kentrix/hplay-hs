{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables, RecordWildCards #-}
module Downloader where

import Json
import Data.Text
import qualified Network.Download as DL
import System.Timeout
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Internal as BS
import qualified Data.ByteString as BS
import System.FilePath.Posix ((</>), makeValid)
import qualified Network.MPD as MPD
import qualified Data.String as S

data DownloadConfig = DownloadConfig { url :: Text, extension :: Text, songName :: Text, songSinger :: Text } deriving(Show)

data DownloadResult = DOk FilePath | DErr (Maybe String) deriving (Show)

musicPath = "/home/e/Music/"

clientDataToDownloadConfig :: ClientData -> Maybe DownloadConfig
clientDataToDownloadConfig cd = do
    a <- rURL cd
    b <- rExt cd
    c <- rName cd
    d <- rSingerName cd
    return $ DownloadConfig a b c d

downloadFile :: DownloadConfig -> IO (DownloadResult)
downloadFile cfg = do
    res <- timeout 600000 . DL.openURI . unpack $ url cfg
    case res of
        Just r -> case r of
            Right bs -> do 
                let p' = (unpack $ songName cfg) ++ "--" ++ (unpack $ songSinger cfg) ++ "." ++ (unpack $ extension cfg)
                {- TODO: More efficient string -}
                    p = musicPath </> p'
                putStrLn p
                BS.writeFile p bs
                return $ DOk $ makeValid p'
            Left e -> do
                putStrLn "Download inner fail"
                return $ DErr $ Just $ show e
        _ -> do 
            putStrLn "Download oter fail"
            return $ DErr Nothing

{-# LANGUAGE OverloadedStrings, DeriveGeneric, BangPatterns, RecordWildCards   #-}

module Json where

import Data.Aeson
import Data.Text
import Control.Monad
import GHC.Generics
import qualified Network.MPD as MPD

data Status = Playing | Paused | Stopped deriving(Generic, Show)

instance ToJSON Status
instance FromJSON Status

data ClientSongItem = 
    ClientSongItem { pos :: Maybe Int, name :: !Text, len :: Int, origin :: !Text } deriving(Generic, Show)

instance ToJSON ClientSongItem
instance FromJSON ClientSongItem

data NotifcationType = 
    NQueued
    | NBumped
    | NStopped
    | NPaused
    | NDeleted
    | NPurged
    deriving(Generic, Show)

instance ToJSON NotifcationType
instance FromJSON NotifcationType

data NotificationItem = 
    NotificationItem { notificationType :: NotifcationType
                     , notificationDetail :: !Text
                     , actor :: !Text 
                     } deriving(Generic, Show)

instance ToJSON NotificationItem
instance FromJSON NotificationItem

data ServerData =
    ServerData { iType :: InstructionType
               , iDetail :: Maybe Text
               , iSongList :: Maybe [ClientSongItem]
               , iNotification :: Maybe NotificationItem 
               , iLibrary :: Maybe [ClientSongItem]
               , iPlayerStatus :: Maybe Status
               , iVolume :: Maybe Int
               , iCurrTime :: Maybe Int
               , iCurrPos :: Maybe Int
               } deriving(Generic, Show)

instance ToJSON ServerData
instance FromJSON ServerData


data ClientData = 
    ClientData { rType :: RequestType, rURL :: Maybe Text, rInt :: Maybe Int, rExt :: Maybe Text, rName :: Maybe Text, rSingerName :: Maybe Text} deriving(Generic, Show)

instance ToJSON ClientData
instance FromJSON ClientData where
    parseJSON = withObject "client data" $ \o -> do
        rType <- o .: "rType"
        rURL <- o .:? "rURL"
        rInt <- o .:? "rInt"
        rExt <- o .:? "rExt"
        rName <- o .:? "rName"
        rSingerName <- o .:? "rSingerName"
        return ClientData{..}

data InstructionType =
    IUpdate
    | INotification
    | IPlay
    | IPause
    | IStop
    | IVolume
    | ISeek
    | IError
    deriving (Generic, Show)

instance ToJSON InstructionType
instance FromJSON InstructionType

data RequestType =
    RUpdate
    | RNew
    | RPlay
    | RPause
    | RStop
    | RVolume
    | RSeek
    | RQueue
    | RHello
    | RDel
    deriving (Generic, Show)

instance ToJSON RequestType
instance FromJSON RequestType

{-
processRequestType :: Text -> Maybe RequestType
processRequestType s =
    case s of
        "update" -> Just RUpdate
        "new" -> Just RNew
        "play" -> Just RPlay
        "pause" -> Just RPause
        "stop" -> Just RStop
        "volume" -> Just RVolume
        _ -> Nothing

processClientData :: ClientData -> Maybe (RequestType, Text)
processClientData d = do
    type_ <- processRequestType $ requestType d
    return (type_, detail d) -}
